function checkValue(value: any){
	if (Array.isArray(value)) return value.length > 0
	else if (value !== null && value !== undefined) {
		if (typeof value === 'string') return value.replace(/^\s+|\s+$/g, '') !== '';
		return true;
	}
	return false;
}

function cutString(value: string): string{
    let arr = value.split(" ");
    if(arr.length > 4)	return arr.slice(0, 4).join(" ").concat("...");
    return value;
  }

export { checkValue, cutString };