export interface ModalProps {
  heading: string;
  body: string;
  clickHandler: () => void;   
}