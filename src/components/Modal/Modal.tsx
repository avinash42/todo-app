import React from 'react';

import { ModalProps } from './Modal.d';

const Modal: React.FC<ModalProps> = ({
    heading,
    body,
    clickHandler
}) => {
    return(
        <div className="modal" id="myModal">
            <div className="modal-dialog">
                <div className="modal-content">

                <div className="modal-header">
                    <h4 className="modal-title">{heading}</h4>
                    <button type="button" className="close" data-dismiss="modal">&times;</button>
                </div>

                <div className="modal-body">
                    {body}
                </div>

                <div className="modal-footer">
                    <button type="button" className="btn btn-danger" data-dismiss="modal" onClick={clickHandler}>Yes</button>
                    <button type="button" className="btn btn-danger" data-dismiss="modal">No</button>
                </div>

                </div>
            </div>
        </div>
    )
}

export default Modal;