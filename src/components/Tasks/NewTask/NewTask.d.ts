export interface NewTaskProps {
    newTaskChangeHandler: (event: ChangeEvent<HTMLInputElement>) => void;
    newTask: string;
    addNewTaskHandler: () => void;
}

