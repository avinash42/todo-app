import React from 'react';
import { NewTaskProps } from './NewTask.d';

const InputField: React.FC<NewTaskProps> = ({
    newTaskChangeHandler,
    newTask,
    addNewTaskHandler
}) => (
    <>
        <input 
            type="text" 
            name="new-task" title="new-task"
            className="new-task-input" 
            onChange={newTaskChangeHandler} 
            value={newTask} 
            onKeyDown={e => { if(e.keyCode === 13) addNewTaskHandler(); }}
            placeholder="write new task and press enter"
            autoFocus={true}
        />
        <button onClick={addNewTaskHandler} className="btn btn-sm btn-primary">ADD</button>
    </>
);

export default InputField;