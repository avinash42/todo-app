export interface TaskProps {
    isUpdate: { bool: boolean, index: number, description: string, status: string };
    setIsUpdate: ({ bool: boolean, index: number, description: string, status: string }) => void;
    index: number;
    changeHandler: (event: ChangeEvent<HTMLInputElement>) => void;
    statusChangeHandler: (event: ChangeEvent<HTMLSelectElement>) => void;
    updateHandler: (index: number) => void;
    task: { description: string, status: string };
    setDeleteTask: ({isModalShown: boolean, description: string, index: number}) => void;
}