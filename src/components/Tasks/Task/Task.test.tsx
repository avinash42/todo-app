import '@testing-library/jest-dom';
import React from 'react';
import { render, cleanup, fireEvent, screen } from '@testing-library/react';
import { createStore } from 'redux';
import { Provider } from 'react-redux';

import { initialState, taskReducer } from '../../../redux-builder/reducer';
import Task from './Task';

const renderWithRedux = (
    Task,
    { initialState, store = createStore(taskReducer, initialState)} = {}
) => {
    return {
        ...render(<Provider store={store}>{Task}</Provider>)
    }
}

afterEach(cleanup);

describe("Task", () => {
    test('renders Task component', () => {
        const { getByTestId } = renderWithRedux(<Task />);
        // succeeds
        expect(getByTestId("clickHandler")).toHaveTextContent("EDIT");
    });    
});

afterEach(cleanup);

test('calls correct function on click', () => {
    const onClick = jest.fn();
    const { getByText } = render(<Button onClick={onClick} />)
    fireEvent.click(getByText(defaultProps.text));
    expect(onClick).toHaveBeenCalled();
    // render(<Task />);
    // const editButton = screen.getAllByText("EDIT")
    // fireEvent.click(editButton[1])

    // let users = store.getState().usersRes.users
    // expect(users.length).toBe(1);
    // fireEvent.click(getByTestId("changeHandler"));
    // expect(getByTestId('button-up')).not.toHaveAttribute('disabled')
})

afterEach(cleanup);

it('should be enabled', () => {
    const { getByTestId } = render(<Task />);
    expect(getByTestId('clickHandler')).not.toHaveAttribute('disabled');
});

afterEach(cleanup);
/*
it(" tasks description ", () => {
    render(<Task />);
    let descriptionInput: any = screen.queryByTitle("description")
    fireEvent.change(descriptionInput, { target: { value: 'Test' } });
    expect(descriptionInput.value).toBe("Test")
})
*/