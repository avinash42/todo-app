import React, { useCallback, useEffect } from 'react';

import { TaskProps } from './Task.d';
import { cutString, checkValue } from '../../../commons';

const Task: React.FC<TaskProps> = ({
  isUpdate,
  index,
  changeHandler,
  statusChangeHandler,
  updateHandler,
  setIsUpdate,
  task,
  setDeleteTask
}) => {
  useEffect(() => {
    // componentDidUpdate
    // console.log(" task ", task);
  }, [task]);

    return(
    <li className="single-task">
      {(checkValue(isUpdate) && isUpdate.bool && isUpdate.index === index) ? 
        <>
          <input type="text" value={isUpdate.description}  
            className="edit-task-input" 
            name="description" title="description"
            onChange={changeHandler} 
          /> 
          <select id="status" onChange={statusChangeHandler} value={isUpdate.status}>
            <option value="" disabled>Select</option>
            <option value="inProgress">In Progress</option>
            <option value="done">Done</option>
          </select>
          <div className="btns">
            <button type="button" className="btn btn-sm btn-primary marR"  onClick={() => updateHandler(index)}>SAVE</button>
            <button className="btn btn-sm btn-primary" onClick={() => setIsUpdate({ bool: false, index: -1, description: "", status: ""})}>CANCEL</button>
          </div>
        </>
        :
        <>
          {checkValue(task) && <>
            <div className={`padR ${task.status === "done"? " done": ""}`}>
              {checkValue(task.description) && cutString(task.description)}
              <small className="w3-tag">
                {task.status === "done" ? "DONE": "IN PROGRESS"}
              </small>
            </div>
            <div className="btns">
              <button type="button" className="btn btn-sm btn-primary marR" data-testid="clickHandler" name="clickHandler" 
                onClick={() => setIsUpdate({ bool: true, index, description: task.description, status: task.status })}>EDIT</button>    
              {task.status === "inProgress" && <button type="button" className=" btn btn-sm btn-danger" 
                data-toggle="modal" data-target="#myModal"
                onClick={() => setDeleteTask({isModalShown: true, description: task.description, index })}>x</button>}
            </div>
            </>}
        </>
      }
    </li>
  )
}

// export default React.memo(Task);
export default Task;