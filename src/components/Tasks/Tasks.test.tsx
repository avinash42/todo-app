import React from 'react';
import '@testing-library/jest-dom';
import { render, cleanup, fireEvent, screen } from '@testing-library/react';
import TasksWrapper from './Tasks';
import NewTask from './NewTask/NewTask';

afterEach(cleanup);

it('should take a snapshot', () => {
    const { asFragment } = render(<TasksWrapper />);
    render(<TasksWrapper />);
    expect(asFragment(<TasksWrapper />)).toMatchSnapshot();
});

afterEach(cleanup);

it(" new task input field ", () => {
    render(<NewTask />)
    let newTaskInput: any = screen.queryByTitle("new-task")
    fireEvent.change(newTaskInput, { target: { value: 'Test' } });
    expect(newTaskInput.value).toBe("Test")
})