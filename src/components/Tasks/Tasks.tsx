import React, { useState, useMemo, useCallback, useEffect } from 'react';
import { createSelectorHook, useDispatch } from 'react-redux';
 
import { checkValue } from '../../commons';
import Task from './Task/Task';
import Modal from '../Modal/Modal';
import './Tasks.css';
import NewTask from './NewTask/NewTask';
import { Provider } from 'react-redux';
import store from '../../redux/store';
import { addTask, updateTask, deleteTask as deleteTaskAction } from '../../redux/taskReducer';

const TasksMain: React.FC = () =>   {
    const [isUpdate, setIsUpdate] = useState({ bool: false, index: -1, description: "", status: "inProgress"});
    const [newTask, setNewTask] = useState("");
    const [deleteTask, setDeleteTask] = useState({isModalShown: false, description: "", index: -1});
    const dispatch = useDispatch();
    const useSelector = createSelectorHook<[]>()
    let tasks : {
        description: string;
        status:  string,
        id: string
    }[] = useSelector(state => state) || [];

    useEffect(() => {
        // console.log(" tasks ", tasks);
    }, [tasks]);

    const deleteHandler = useCallback(() => {
        if(deleteTask.index > -1) dispatch(deleteTaskAction({ index: deleteTask.index }));
    }, [dispatch, deleteTask.index])
    
    const updateHandler = useCallback((index: number) => {
        let tasksUpdate = [...tasks];
        // tasksUpdate[index].description = isUpdate.description, 
        // tasksUpdate[index].status = isUpdate.status;
        // dispatch(updateTask({ index, tasksUpdate }));
        dispatch(updateTask(isUpdate));
        // dispatch(updateTask(isUpdate.description));

        setIsUpdate({ bool: false, index: -1, description: "", status: ""});
    }, [dispatch, isUpdate]);

    function changeHandler(event: React.ChangeEvent<HTMLInputElement>): void{
        if(checkValue(event.currentTarget.value)){
            let isUpdateCopy = { ...isUpdate };
            isUpdateCopy.description = event.currentTarget.value;
            setIsUpdate(isUpdateCopy);
        }
    }
    
    function statusChangeHandler(event: React.ChangeEvent<HTMLSelectElement>): void {
        // console.log(event.currentTarget.value);
        if(checkValue(event.currentTarget.value)){
            let isUpdateCopy = { ...isUpdate };
            isUpdateCopy.status = event.currentTarget.value;
            setIsUpdate(isUpdateCopy);
        }
    }
    
    function newTaskChangeHandler(e: React.ChangeEvent<HTMLInputElement>):void {
        if(checkValue(e.target.value)) setNewTask(e.target.value);
    }

    const addNewTaskHandler = useCallback(() => {
        if(newTask !== "" && newTask !== null && newTask !== undefined){
            dispatch(addTask(newTask));
            setNewTask("");
        }
    }, [dispatch, newTask]);

    const memoizedInputField = useMemo(() => {
        return(<NewTask 
            newTaskChangeHandler={newTaskChangeHandler}
            newTask={newTask}
            addNewTaskHandler={addNewTaskHandler}            
        />)
    }, [newTask])

    // console.log(" tasks ", tasks, " type ", typeof tasks);
    return(
        <div className="tasks-app">
            <h1>TypeScript Todo List</h1>
            {memoizedInputField}          
            {
                (checkValue(tasks)) ? 
                <>
                <ul className="flex-container column">
                    {tasks.map((task: { description: string; status: string; id: string; }, index: number) => 
                        <Task isUpdate={isUpdate} index={index} 
                            changeHandler={changeHandler}
                            key={`task-${index}`}
                            statusChangeHandler={statusChangeHandler}
                            updateHandler={updateHandler}
                            setIsUpdate={setIsUpdate}
                            task={task}
                            setDeleteTask={setDeleteTask}
                        />
                    )}
                <Modal body={deleteTask.description} heading="Delete a Task" 
                    clickHandler={deleteHandler}/>
                </ul>
                </>
                : <p>no data</p>
            }          
        </div>
    )
}

    
const TasksWrapper = () => (
    <Provider store={store}>
        <TasksMain />
    </Provider>
)

export default TasksWrapper;
  