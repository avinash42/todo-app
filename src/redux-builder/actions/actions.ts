import { Task } from '../reducer';
import { addTask } from './addTask';
import { deleteTask } from './deleteTask';
import { updateTask } from './updateTask';

// enum containing the action types
export enum tasksActionTypes {
    addTask = "addTask",
    deleteTask = "deleteTask",
    updateTask = "updateTask"
}

export interface hasTask {
    task: Task,
    index: -1,
    tasksUpdate: []
}

export type tasksAction = addTask | deleteTask | updateTask;

