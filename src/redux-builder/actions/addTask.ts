import { createAction } from '@reduxjs/toolkit';
import { tasksState } from '../reducer';
import { hasTask, tasksActionTypes } from './actions';

export type addTask = {
    type: tasksActionTypes.addTask;
    payload: hasTask;
}

const action = createAction<hasTask, tasksActionTypes.addTask>(
    tasksActionTypes.addTask
);

export const reducer = (
    state:  tasksState,
    { payload }: { payload: hasTask }
) => {
    state.push(payload.task);
};

export default action;