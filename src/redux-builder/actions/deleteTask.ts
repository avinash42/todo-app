import { createAction } from '@reduxjs/toolkit';
import { tasksState } from '../reducer';
import { hasTask, tasksActionTypes } from './actions';

export type deleteTask = {
    type: tasksActionTypes.deleteTask
}

const action = createAction<hasTask, tasksActionTypes.deleteTask>(
    tasksActionTypes.deleteTask
);

export const reducer = (
    state:  tasksState,
    { payload }: { payload: hasTask }
) => {
  //  console.log(" delete 1 ", payload, " state ", state);
    if(payload.index > -1) state.splice(payload.index, 1);
//    console.log(" delete 2  state ", state);
};

export default action;