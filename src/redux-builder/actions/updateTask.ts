import { createAction } from '@reduxjs/toolkit';
import { tasksState } from '../reducer';
import { hasTask, tasksActionTypes } from './actions';

export type updateTask = {
    type: tasksActionTypes.updateTask
}

const action = createAction<hasTask, tasksActionTypes.updateTask>(
    tasksActionTypes.updateTask
);

export const reducer = (    
    state:  tasksState,
    { payload }: { payload: hasTask }
) => {
    state.splice(0, state.length);
    state.push(...payload.tasksUpdate);
};

export default action;