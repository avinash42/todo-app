import { createReducer, Reducer } from '@reduxjs/toolkit';
import { tasksAction } from './actions/actions';
import addTask, { reducer as addTaskReducer } from './actions/addTask';
import deleteTask, { reducer as deleteTaskReducer } from './actions/deleteTask';
import updateTask, { reducer as updateTaskReducer } from './actions/updateTask';

export type Task = {
    description: string;
    status:  string
}

export const initialState = [
    { description: "working on typescript", status: "inProgress" }, 
    { description: "working on javascript", status: "done" }, 
    { description: "riding bike", status: "inProgress" }, 
    { description: "reading a book", status: "inProgress" }, 
    { description: "reading another book", status: "inProgress" }
];

export type tasksState = typeof initialState;

export const taskReducer: Reducer<
    tasksState,
    tasksAction
> = createReducer(
    initialState,

     (builder) => {
        return(
            builder
            .addCase(addTask, addTaskReducer)
            .addCase(deleteTask, deleteTaskReducer)
            .addCase(updateTask, updateTaskReducer)
        )
    }
);

