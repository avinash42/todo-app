import { combineReducers } from 'redux';
import { configureStore } from '@reduxjs/toolkit';
import taskReducer from './taskReducer';


const store = configureStore({ reducer: taskReducer.reducer });

export default store;