import { createSlice, PayloadAction, nanoid } from '@reduxjs/toolkit';

import TodoList from '../todo-list';

const initialState = [
    { description: "working on typescript", status: "inProgress", id: "default-task-1" }, 
    { description: "working on javascript", status: "done", id: "default-task-2" }, 
    { description: "riding bike", status: "inProgress", id: "default-task-3" }, 
    { description: "reading a book", status: "inProgress", id: "default-task-4" }, 
    { description: "reading another book", status: "inProgress", id: "default-task-5" }
];

const slice = createSlice({
    name: "tasks",
    initialState,
    reducers: { 
        addTask:{
            reducer: (state, action: PayloadAction<TodoList>) => {
               console.log(" add reducer ", state, " action ", action);
               const stateClone = state.slice();
               stateClone.push(action.payload);
            }, 
            prepare: (description: string) => {
                console.log(" add prepare ", description);
                return { payload: { description, status: "inProgress", id: nanoid() }}
            }   
        },
        deleteTask: (state, action) => {
            if(action.payload.index > -1)
                state.splice(action.payload.index, 1);
        },
        updateTask: (state, action) => {
            // console.log(JSON.stringify(state))
            // console.log(" update state ", state, " length ", state.length);
            // state.splice(0, state.length);
            // console.log(" update action ", action, " isupdate  ", action.payload.isUpdate);
            let stateClone = JSON.parse(JSON.stringify(state));
          //  console.log(" clone ", stateClone[0]);
            stateClone[action.payload.index].description = action.payload.description;
            stateClone[action.payload.index].status = action.payload.status;
            // console.log(" after operation clone ", stateClone[0]);
            // state.push(action.payload.tasksUpdate);       
        }
    }
})

// Reducer
export default slice;

export const { addTask, deleteTask, updateTask } = slice.actions;