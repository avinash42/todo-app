export default interface TodoList {
    description: string;
    status:  string,
    id: string
}